package telegram

import (
	"fmt"
	"github.com/samber/lo"
	"gitlab.com/disappointment-industries/ingatlan-scraper/db"
	"gitlab.com/disappointment-industries/ingatlan-scraper/mapsapi"
	tele "gopkg.in/telebot.v3"
	"strconv"
	"strings"
	"time"
)

func callback(c tele.Context) error {
	q := c.Callback()

	parts := strings.Split(strings.TrimSpace(q.Data), "|")
	unique := parts[0]
	data := parts[1]

	albid, e := strconv.ParseUint(unique, 10, 0)
	if e != nil {
		return e
	}
	albi, e := db.GetAlbi(uint(albid))
	if e != nil {
		return e
	}

	switch data {
	case "history":
		return handleHistory(c, q.ID, &albi)
	case "location":
		return handleLocation(c, q.ID, &albi)
	}

	return c.Respond(&tele.CallbackResponse{
		CallbackID: q.ID,
		Text:       "Command type not supported",
		ShowAlert:  false,
	})
}

func handleLocation(c tele.Context, id string, albi *db.Albi) error {
	lat, lng, e := mapsapi.Geocode(albi.Addr)
	if e != nil {
		return e
	}
	loc := &tele.Location{
		Lat: lat,
		Lng: lng,
	}
	e = c.Reply(loc)
	if e != nil {
		return e
	}
	return c.Respond(&tele.CallbackResponse{
		CallbackID: id,
		Text:       "Location sent!",
	})
}

func handleHistory(c tele.Context, id string, albi *db.Albi) error {
	chat, e := db.GetChat(c.Chat().ID)
	if e != nil {
		return e
	}

	loc, e := time.LoadLocation(chat.Location)
	if e != nil {
		_ = c.Send("The timezone of this chat is not set, use /timezone to set it (using UTC for the time being)")
		loc = time.UTC
	}

	msg := strings.Join(lo.Map(albi.PriceHistory, func(item *db.OldPrice, _ int) string {
		if !item.Price.Valid {
			return fmt.Sprintf("missing @ %s", item.ChangeTime.In(loc).Format("01-02 15:04"))
		}
		return fmt.Sprintf("%dFt @ %s", item.Price.Int64, item.ChangeTime.In(loc).Format("01-02 15:04"))
	}), "\n")

	return c.Respond(&tele.CallbackResponse{
		CallbackID: id,
		Text:       msg,
		ShowAlert:  true,
	})
}
