package telegram

import (
	"github.com/cenkalti/backoff/v4"
	"github.com/samber/lo"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/ingatlan-scraper/common"
	"gitlab.com/disappointment-industries/ingatlan-scraper/db"
	tele "gopkg.in/telebot.v3"
	"time"
)

var b *tele.Bot

func init() {
	pref := tele.Settings{
		Token:  env.GetOrDosomething("TOKEN", func() string { panic("TOKEN env var missing") }),
		Poller: &tele.LongPoller{Timeout: 10 * time.Second},
	}

	var err error
	b, err = tele.NewBot(pref)
	if err != nil {
		panic(err)
	}

	err = b.SetCommands([]tele.Command{
		{
			Text:        "/start",
			Description: "Dobd ide is",
		},
		{
			Text:        "/stop",
			Description: "Hagyd abba pls",
		},
		{
			Text:        "/timezone",
			Description: "Időzona váltás",
		},
	})
	if err != nil {
		panic(err)
	}

	b.Handle("/start", func(c tele.Context) error {
		debug := len(c.Args()) == 1 && c.Args()[0] == "debug"
		e := db.SaveChat(db.Chat{
			ID:    c.Chat().ID,
			Debug: debug,
		})
		if e != nil {
			return e
		}

		if debug {
			return c.Send("Jujci, magic flag 🪄", tele.ModeHTML)
		}

		return c.Reply("Hey, mostantól ide is fogom dobálni a notikat", tele.ModeHTML)
	})

	b.Handle("/stop", func(c tele.Context) error {
		e := db.DelChat(db.Chat{ID: c.Chat().ID})
		if e != nil {
			return e
		}
		return c.Reply("Mostantól nem dobom ide az üziket", tele.ModeHTML)
	})

	b.Handle("/timezone", func(c tele.Context) error {
		if len(c.Args()) != 1 {
			return c.Reply("usage: /timezone <timezone>")
		}

		ch, e := db.GetChat(c.Chat().ID)
		if e != nil {
			return e
		}

		ch.Location = c.Args()[0]

		_, e = time.LoadLocation(ch.Location)
		if e != nil {
			return c.Reply(e.Error())
		}

		e = db.SaveChat(ch)
		if e != nil {
			return e
		}

		return c.Reply("Success", tele.ModeHTML)
	})

	b.Handle(tele.OnCallback, callback)

	go b.Start()
}

func getChats() ([]db.Chat, error) {
	chats, e := db.GetAllChats()
	if e != nil {
		return nil, e
	}

	if *common.Debug {
		chats = lo.Filter(chats, func(item db.Chat, _ int) bool {
			return item.Debug
		})
	} else {
		chats = lo.Filter(chats, func(item db.Chat, _ int) bool {
			return !item.Debug
		})
	}
	return chats, nil
}

func SendMessage(albis chan db.Albi) error {
	for albi := range albis {
		chats, e := getChats()
		if e != nil {
			return e
		}

		for _, chat := range chats {
			c, e := b.ChatByID(chat.ID)
			if e != nil {
				// todo forget chat after a couple of failures
				continue
			}

			e = backoff.Retry(func() error {
				return SendAlbi(c, albi)
			}, backoff.NewConstantBackOff(time.Second*5))

			if e != nil {
				return e
			}
		}
	}

	return nil
}
