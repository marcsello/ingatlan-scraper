package telegram

import (
	"bytes"
	"fmt"
	"gitlab.com/disappointment-industries/ingatlan-scraper/db"
	"gopkg.in/telebot.v3"
	"html/template"
)

var tmpl = template.Must(template.New("").Parse(`
<b>💸 {{ .Albi.Price.Int64 }} Ft/hó</b> {{ .Icon }} {{ .Diff }}
<i>📍{{ .Albi.Addr }}</i>
{{ .Albi.Size }} {{ .Albi.Rooms }}
`))

func SendAlbi(c *telebot.Chat, a db.Albi) error {
	markup := &telebot.ReplyMarkup{}
	url := markup.URL("🔗 Link", a.URL)
	history := markup.Data("📈 History", fmt.Sprint(a.ID), "history")
	loc := markup.Data("🗺️ Location", fmt.Sprint(a.ID), "location")
	markup.Inline(
		markup.Row(url, history, loc),
	)

	icon := "🆕"
	diff := ""

	if len(a.PriceHistory) >= 2 {
		n := a.PriceHistory[0].Price
		o := a.PriceHistory[1].Price
		if !o.Valid && n.Valid {
			icon = "♻️"
		} else if o.Valid && !n.Valid {
			icon = "🗑️"
		} else if n.Int64 > o.Int64 {
			icon = "📈"
			diff = fmt.Sprintf("+%dFt", n.Int64-o.Int64)
		} else {
			icon = "📉"
			diff = fmt.Sprintf("-%dFt", o.Int64-n.Int64)
		}
	}

	var buf bytes.Buffer
	e := tmpl.Execute(&buf, map[string]any{
		"Albi": a,
		"Icon": icon,
		"Diff": diff,
	})
	if e != nil {
		return e
	}

	if a.ThumbnailURL != "" {
		photo := &telebot.Photo{File: telebot.FromURL(a.ThumbnailURL)}
		photo.Caption = buf.String()
		_, e = b.Send(c, photo, telebot.ModeHTML, markup)
	} else {
		_, e = b.Send(c, buf.String(), telebot.ModeHTML, markup)
	}

	return e
}
