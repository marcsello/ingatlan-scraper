package main

import (
	"flag"
	"fmt"
	"gitlab.com/disappointment-industries/ingatlan-scraper/common"
	"gitlab.com/disappointment-industries/ingatlan-scraper/db"
	"gitlab.com/disappointment-industries/ingatlan-scraper/scraper"
	"gitlab.com/disappointment-industries/ingatlan-scraper/telegram"
	"log"
	"time"
)

func main() {
	defer time.Sleep(time.Second)
	flag.Parse()

	albiChan := make(chan db.Albi)
	go func() {
		fmt.Println("Starting ingatlan.com scraper...")
		defer fmt.Println("Scraper for ingatlan.com stopped")
		for t := range common.NewTicker() {
			fmt.Println("Started ingatlan.com at", t)
			err := scraper.GetIngatlanCom(albiChan)
			if err != nil {
				log.Println(err)
			}
		}
	}()

	go func() {
		fmt.Println("Starting jofogas scraper...")
		defer fmt.Println("Scraper for jofogas stopped")
		for t := range common.NewTicker() {
			fmt.Println("Started jofogas at", t)
			err := scraper.GetJofogas(albiChan)
			if err != nil {
				log.Println(err)
			}
		}
	}()

	go func() {
		fmt.Println("Starting janitor...")
		defer fmt.Println("Janitor stopped")
		for t := range common.NewTicker() {
			fmt.Println("Started janitor at", t)
			soft, hard, err := db.AlbiCleanup()
			fmt.Printf("Janitor: marked %d, deleted %d\n", soft, hard)
			if err != nil {
				log.Println(err)
			}
		}
	}()

	changed := make(chan db.Albi)
	go func() {
		fmt.Println("Starting DB differ...")
		defer fmt.Println("DB differ stopped")
		err := db.SaveAlbis(albiChan, changed)
		if err != nil {
			panic(err)
		}
	}()

	fmt.Println("Starting Telegram bot")
	defer fmt.Println("Telegram bot stopped")
	err := telegram.SendMessage(changed)
	if err != nil {
		panic(err)
	}
}
