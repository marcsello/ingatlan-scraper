package mapsapi

import (
	"encoding/json"
	"errors"
	"gitlab.com/MikeTTh/env"
	"net/http"
	"net/url"
)

var apiKey = env.GetOrDosomething("MAPS_APIKEY", func() string { panic("MAPS_APIKEY env var missing") })

type MapsResp struct {
	Results []struct {
		Geometry struct {
			Location struct {
				Lat float32 `json:"lat"`
				Lng float32 `json:"lng"`
			} `json:"location"`
		} `json:"geometry"`
	} `json:"results"`
}

func Geocode(in string) (lat, lng float32, e error) {
	query := url.Values{}
	query.Add("key", apiKey)
	query.Add("address", in)
	req, e := http.Get("https://maps.googleapis.com/maps/api/geocode/json?" + query.Encode())
	if e != nil {
		return 0, 0, e
	}
	dec := json.NewDecoder(req.Body)

	var maps MapsResp
	e = dec.Decode(&maps)
	if e != nil {
		return 0, 0, e
	}

	if len(maps.Results) < 1 {
		return 0, 0, errors.New("unknown location")
	}

	loc := maps.Results[0].Geometry.Location

	return loc.Lat, loc.Lng, nil
}
