package db

import (
	"database/sql"
	"time"
)

type OldPrice struct {
	ID uint `gorm:"primarykey"`

	AlbiID     uint
	AlbiSource string `gorm:"type:varchar(3)"`
	ChangeTime time.Time
	Price      sql.NullInt64

	Albi *Albi `gorm:"belongsTo:Albi"`
}

type Albi struct {
	ID           uint        `gorm:"primarykey"`
	Source       string      `gorm:"type:varchar(3);primarykey"`
	CreatedAt    time.Time   `gorm:"default:now()"`
	LastSeen     time.Time   `gorm:"default:now()"`
	PriceHistory []*OldPrice `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`

	Price        sql.NullInt64
	Addr         string `gorm:"type:varchar(200)"`
	Size         string `gorm:"type:varchar(100)"`
	Rooms        string `gorm:"type:varchar(100)"`
	URL          string `gorm:"type:varchar(255)"`
	ThumbnailURL string `gorm:"type:varchar(255)"`
}

type Chat struct {
	ID       int64 `gorm:"primaryKey"`
	Debug    bool
	Location string
}
