package db

import (
	"database/sql"
	"github.com/samber/lo"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"time"
)

const twoDays = time.Hour * 24 * 2
const twoHours = time.Hour * 2

func AlbiCleanup() (int64, int64, error) {
	var fresh Albi
	e := db.Order("last_seen desc").First(&fresh).Error
	if e == gorm.ErrRecordNotFound { // no data yet
		return 0, 0, nil
	}
	if e != nil {
		return 0, 0, e
	}

	// don't clean up if the scraper hasn't run recently
	if fresh.LastSeen.Before(time.Now().Add(-time.Hour)) {
		return 0, 0, nil
	}

	// set price to null if not seen for two hours
	var softs []Albi
	e = db.Transaction(func(db *gorm.DB) error {
		er := db.Model(&softs).Clauses(clause.Returning{}).
			Where("last_seen < ?", time.Now().Add(-twoHours)).
			Where("price notnull").
			Updates(&Albi{
				Price: sql.NullInt64{Int64: 100, Valid: false}, // set price to null
			}).Error
		if er != nil && er != gorm.ErrRecordNotFound {
			return er
		}
		op := lo.Map(softs, func(item Albi, _ int) OldPrice {
			return OldPrice{
				AlbiID:     item.ID,
				AlbiSource: item.Source,
				ChangeTime: time.Now(),
				Price:      sql.NullInt64{Valid: false},
			}
		})
		if len(op) > 0 {
			er = db.Save(&op).Error
			if er != nil {
				return er
			}
		}
		return nil
	})
	if e != nil {
		return 0, 0, e
	}

	// delete if not seen for two days
	hard := db.Where("last_seen < ?", time.Now().Add(-twoDays)).Delete(&Albi{})
	if hard.Error != nil && hard.Error != gorm.ErrRecordNotFound {
		return int64(len(softs)), hard.RowsAffected, e
	}

	return int64(len(softs)), hard.RowsAffected, nil
}
