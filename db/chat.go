package db

func SaveChat(c Chat) error {
	return db.Save(&c).Error
}

func DelChat(c Chat) error {
	return db.Delete(&c).Error
}

func GetAllChats() ([]Chat, error) {
	var chats []Chat
	e := db.Find(&chats).Error
	return chats, e
}

func GetChat(id int64) (Chat, error) {
	chat := Chat{ID: id}
	e := db.Find(&chat).Error
	return chat, e
}
