package db

import (
	"gitlab.com/MikeTTh/env"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

var db *gorm.DB

func init() {
	dsn := env.String("POSTGRES", "postgresql://localhost/postgres")
	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.New(
			log.New(os.Stdout, "\r\n", log.LstdFlags),
			logger.Config{
				SlowThreshold:             time.Second,
				LogLevel:                  logger.Warn,
				IgnoreRecordNotFoundError: true,
				Colorful:                  true,
			},
		),
	})
	if err != nil {
		panic(err)
	}

	e := db.AutoMigrate(&Albi{}, &OldPrice{}, &Chat{})
	if e != nil {
		panic(e)
	}
}
