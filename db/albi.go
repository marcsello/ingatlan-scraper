package db

import (
	"gorm.io/gorm"
	"math"
	"time"
)

func SaveAlbis(albis, changed chan Albi) error {
	for albi := range albis {
		err := db.Transaction(func(tx *gorm.DB) error {
			prev := Albi{ID: albi.ID, Source: albi.Source}

			e := tx.Preload("PriceHistory", func(db *gorm.DB) *gorm.DB {
				return db.Order("change_time DESC")
			}).First(&prev).Error
			if e != nil && e != gorm.ErrRecordNotFound {
				tx.Rollback()
				return e
			}

			if prev.CreatedAt.IsZero() {
				albi.CreatedAt = time.Now()
			} else {
				albi.CreatedAt = prev.CreatedAt
			}

			albi.LastSeen = time.Now()

			if e := tx.Save(&albi).Error; e != nil {
				tx.Rollback()
				return e
			}

			if prev.Price != albi.Price {
				op := OldPrice{
					ChangeTime: time.Now(),
					Price:      albi.Price,
					AlbiID:     albi.ID,
					AlbiSource: albi.Source,
				}

				if e := tx.Save(&op).Error; e != nil {
					tx.Rollback()
					return e
				}

				albi.PriceHistory = append([]*OldPrice{&op}, prev.PriceHistory...)

				// only notify on significant change
				if math.Abs(float64(prev.Price.Int64-albi.Price.Int64)) >= 10000 {
					changed <- albi
				}
			}
			return nil
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func GetAlbi(id uint) (Albi, error) {
	a := Albi{ID: id}
	e := db.Preload("PriceHistory", func(db *gorm.DB) *gorm.DB {
		return db.Order("change_time DESC")
	}).First(&a).Error
	return a, e
}
