FROM alpine
ARG TARGETARCH
WORKDIR /app
RUN apk add --no-cache tzdata
COPY app-${TARGETARCH} /app/ingatlan
ENTRYPOINT [ "/app/ingatlan" ]
