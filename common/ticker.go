package common

import (
	"github.com/lthibault/jitterbug/v2"
	"time"
)

func NewTicker() <-chan time.Time {
	t := jitterbug.New(
		time.Minute*5,
		&jitterbug.Norm{Stdev: time.Minute * 1},
	)

	c := make(chan time.Time)

	go func() {
		for tick := range t.C {
			c <- tick
		}
	}()

	go func() {
		c <- time.Now()
	}()

	return c
}
