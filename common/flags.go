package common

import "flag"

var Debug = flag.Bool("debug", false, "Run in debug mode")
