package common

import (
	"database/sql"
	"golang.org/x/exp/constraints"
)

func ToNullInt64[T constraints.Integer](in T) sql.NullInt64 {
	return sql.NullInt64{
		Int64: int64(in),
		Valid: true,
	}
}
