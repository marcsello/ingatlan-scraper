package scraper

import (
	"fmt"
	"github.com/gocolly/colly/v2"
	"github.com/samber/lo"
	"gitlab.com/disappointment-industries/ingatlan-scraper/common"
	"gitlab.com/disappointment-industries/ingatlan-scraper/db"
	"strconv"
	"strings"
	"time"
)

const ingatlanUrl = "https://ingatlan.com/szukites/kiado+lakas+tegla-epitesu-lakas+panel-lakas+csuszozsalus-lakas+ar-szerint+i-ker+ix-ker+xi-ker+xii-ker+xiii-ker+havi-120-190-ezer-Ft+2-szoba-felett"

func GetIngatlanCom(albiChan chan db.Albi) error {
	c := colly.NewCollector()

	c.OnHTML("div.listing__card", func(e *colly.HTMLElement) {
		price := e.ChildText("div.price")
		img := e.ChildAttr("img.listing__image", "src")
		addr := e.ChildText("div.listing__address")
		size := e.ChildText("div.listing__data--area-size")
		rooms := e.ChildText("div.listing__data--room-count")
		url := e.ChildAttr("a.listing__link", "href")
		id, _ := lo.Last(strings.Split(url, "/"))
		intid, err := strconv.ParseUint(id, 10, 0)
		if err != nil {
			panic(err)
		}

		price = strings.ReplaceAll(price, " ", "")
		price = strings.ReplaceAll(price, "Ft/hó", "")

		pri, err := strconv.ParseUint(price, 10, 0)
		if err != nil {
			panic(err)
		}

		u := e.Request.URL
		albiChan <- db.Albi{
			Source: "ING",
			ID:     uint(intid),

			Price: common.ToNullInt64(pri),
			Addr:  addr,
			Size:  size,
			Rooms: rooms,

			URL:          fmt.Sprintf("%s://%s%s", u.Scheme, u.Host, url),
			ThumbnailURL: img,
		}
	})

	c.OnHTML("div.pagination__page-number", func(e *colly.HTMLElement) {
		fmt.Println("Ingatlan.com:", e.Text)
	})

	c.OnHTML("a.pagination__button", func(e *colly.HTMLElement) {
		if e.Text == "Következő oldal" {
			time.Sleep(time.Second)
			_ = e.Request.Visit(e.Attr("href"))
		}
	})

	err := c.Visit(ingatlanUrl)
	if err != nil {
		return err
	}

	return nil
}
