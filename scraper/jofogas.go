package scraper

import (
	"fmt"
	"github.com/gocolly/colly/v2"
	"github.com/samber/lo"
	"gitlab.com/disappointment-industries/ingatlan-scraper/common"
	"gitlab.com/disappointment-industries/ingatlan-scraper/db"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const jofogasUrl = "https://ingatlan.jofogas.hu/budapest/i-kerulet+iv-kerulet+ix-kerulet+v-kerulet+vi-kerulet+vii-kerulet+xi-kerulet+xii-kerulet+xiii-kerulet/lakas?max_price=190000&min_price=120000&ros=2&sp=1&st=u"

func GetJofogas(albiChan chan db.Albi) error {
	c := colly.NewCollector()

	c.OnHTML("div.list-item", func(e *colly.HTMLElement) {
		price := e.ChildText("span.price-value")
		img := e.ChildAttr("img[loading=lazy]", "src")
		addr := strings.ReplaceAll(e.ChildText("section.cityname"), "  ,", ",")
		size := e.ChildText("div.size")
		rooms := e.ChildText("div.rooms")
		adUrl := e.ChildAttr("a.subject", "href")
		id, _ := lo.Last(strings.Split(adUrl, "_"))
		id = strings.TrimSuffix(id, ".htm")
		intid, err := strconv.ParseUint(id, 10, 0)
		if err != nil {
			panic(err)
		}

		price = strings.ReplaceAll(price, " ", "")
		pri, err := strconv.ParseUint(price, 10, 0)
		if err != nil {
			panic(err)
		}

		albiChan <- db.Albi{
			Source: "JOF",
			ID:     uint(intid),

			Price: common.ToNullInt64(pri),
			Addr:  addr,
			Size:  size,
			Rooms: rooms,

			URL:          adUrl,
			ThumbnailURL: img,
		}
	})

	c.OnHTML("a.ad-list-pager-item-last", func(e *colly.HTMLElement) {
		maxNumUrl, er := url.Parse(e.Attr("href"))
		if er != nil {
			return
		}
		maxNum := maxNumUrl.Query().Get("o")

		curNum := e.Request.URL.Query().Get("o")
		if curNum == "" {
			curNum = "1"
		}
		fmt.Printf("Jófogás: %s / %s oldal\n", curNum, maxNum)
	})

	c.OnHTML("span.ad-list-pager-item-last", func(e *colly.HTMLElement) {
		curNum := e.Request.URL.Query().Get("o")
		if curNum == "" {
			curNum = "1"
		}
		fmt.Printf("Jófogás: %s / %s oldal\n", curNum, curNum)
	})

	c.OnHTML("a.ad-list-pager-item-next", func(e *colly.HTMLElement) {
		time.Sleep(time.Second)
		_ = e.Request.Visit(e.Attr("href"))
	})

	err := c.Visit(jofogasUrl)
	if err != nil {
		return err
	}

	return nil
}
